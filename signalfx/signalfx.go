package signalfx

import (
	"context"
	"github.com/signalfx/golib/datapoint"
	"github.com/signalfx/golib/sfxclient"
)

type SignalFx struct {
	DatapointEndpoint string `json:"datapoint_endpoint"`
	EventEndpoint string `json:"event_endpoint"`
	TraceEndpoint string `json:"trace_endpoint"`
	AuthToken string `json:"auth_token"`
}

type Dimension struct {
	Hostname string
	Environment string
	Region string
}

func (signalfx *SignalFx) SendMetric(name string, dimension map[string]string, value int64) (err error){
	var client = sfxclient.NewHTTPSink()
	client.DatapointEndpoint = signalfx.DatapointEndpoint
	client.EventEndpoint = signalfx.EventEndpoint
	client.TraceEndpoint = signalfx.TraceEndpoint
	client.AuthToken = signalfx.AuthToken
	
	var ctx = context.Background()
	err = client.AddDatapoints(ctx,[]*datapoint.Datapoint{
		sfxclient.Gauge(name, dimension, value),
	})
	
	return err
}

func BuildDimension(hostname, environment, region string) map[string]string {
	dimension := make(map[string]string)
	dimension["hostname"] = hostname
	dimension["environment"] = environment
	dimension["region"] = region
	return dimension
}
