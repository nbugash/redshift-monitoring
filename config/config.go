package config

import (
	"github.com/ghodss/yaml"
	"io/ioutil"
	"log"
	"os"
	
	"redshift-monitoring/redshift"
	"redshift-monitoring/signalfx"
)


/*
 * List of constants
 **/
const (
	YamlConfigurationFile = "./config/config.yaml"
)

/*
 * Configuration struct
 */
type Configuration struct {
	Redshifts []redshift.Configuration `json:"redshifts"`
	SignalFx signalfx.SignalFx `json:"signalfx"`
}

func (config *Configuration) Read() error {
	var source, err = ioutil.ReadFile(YamlConfigurationFile)
	if err != nil {
		log.Fatalf("Unable to read the configuration file %s", YamlConfigurationFile)
	}
	// expand environment variables
	source = []byte(os.ExpandEnv(string(source)))
	err = yaml.Unmarshal(source, &config)
	if err != nil {
		log.Fatal("Unable to unmarshal the configuration")
	}
	return err
}
