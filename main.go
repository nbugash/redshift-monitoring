package main

import (
	"fmt"
	"log"
	
	"redshift-monitoring/config"
	"redshift-monitoring/redshift"
	"redshift-monitoring/signalfx"
)
var configuration config.Configuration

func main() {
	var err = configuration.Read()
	if err != nil {
		log.Fatal("Unable to read configuration file")
	}
	signalfxConfig := configuration.SignalFx
	signalfxService := signalfx.SignalFx{
		DatapointEndpoint: signalfxConfig.DatapointEndpoint,
		EventEndpoint: signalfxConfig.EventEndpoint,
		TraceEndpoint: signalfxConfig.TraceEndpoint,
		AuthToken: signalfxConfig.AuthToken,
	}
	for _, clusterInfo := range configuration.Redshifts {
		var numOfStlLoadErrors = redshift.GetNumOfErrors(clusterInfo)
		fmt.Printf("There are %d error(s) found on the %s - %s database\n", numOfStlLoadErrors, clusterInfo.Hostname,clusterInfo.Database)
		stlLoadErrorDim := signalfx.BuildDimension(clusterInfo.Hostname, clusterInfo.Environment, clusterInfo.Region)
		err = signalfxService.SendMetric("stl_load_errors", stlLoadErrorDim, numOfStlLoadErrors)
		if err != nil {
			log.Fatalf("%v", err)
		}
		var results = redshift.GetQueryRuntime(clusterInfo)
		for _, result := range results {
			fmt.Printf("The query \"%s\" ran for %g seconds.\n", result.Sqlquery, result.Duration.Seconds())
		}
	}
}
