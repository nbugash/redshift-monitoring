package redshift

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"time"
)

type Configuration struct {
	Hostname string `json:"hostname" yaml:"hostname"`
	Port     int    `json:"port" yaml:"port"`
	Username string `json:"username" yaml:"username"`
	Password string `json:"password" yaml:"password"`
	Database string `json:"database" yaml:"database"`
	Environment string `json:"environment" yaml:"environment"`
	Region string `json:"region" yaml:"region"`
}

const (
	ConnectionStringFmt = "postgres://%s:%s@%s:%d/%s"
	ErrorsInStlLoadErrors = "select count(*) from stl_load_errors"
	QueryRuntime = `SELECT query, trim(querytxt) AS sqlquery, starttime,
					endtime FROM stl_query ORDER BY starttime DESC LIMIT 10`
)

type QueryRuntimeResult struct {
	Query int
	Sqlquery string
	Starttime time.Time
	Endtime time.Time
	Duration time.Duration
}

func GetNumOfErrors(config Configuration) int64 {
	var numOfErrors int64
	db, err := connect(config)
	if err != nil {
		log.Fatalf("Unable to connect to %s", config.Database)
	}
	defer db.Close()
	err = db.QueryRow(ErrorsInStlLoadErrors).Scan(&numOfErrors)
	if err != nil {
		log.Fatalln("Unable to query the stl_load_errors table")
	}
	return numOfErrors
}

func GetQueryRuntime(config Configuration) []QueryRuntimeResult {
	db, err := connect(config)
	if err != nil {
		log.Fatalf("Unable to connect to %s", config.Database)
	}
	defer db.Close()
	rows, err := db.Query(QueryRuntime)
	if err != nil {
		log.Fatalf("Unable to run the query: %s", QueryRuntime)
	}
	var results []QueryRuntimeResult
	for rows.Next() {
		var result = QueryRuntimeResult{}
		err = rows.Scan(&result.Query, &result.Sqlquery, &result.Starttime, &result.Endtime)
		result.Duration = result.Endtime.Sub(result.Starttime)
		results = append(results, result)
	}
	return results
}

func getConnectionStr(config Configuration) string {
	return fmt.Sprintf(ConnectionStringFmt,
		config.Username, config.Password, config.Hostname, config.Port, config.Database)
}

func connect(config Configuration)(db *sql.DB, err error) {
	return sql.Open("postgres", getConnectionStr(config))
}
